var gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
		sass 					 = require('gulp-sass'),
		sassGlob 			 = require('gulp-sass-glob'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		autoprefixer   = require('gulp-autoprefixer'),
		notify         = require("gulp-notify"),
		livereload 		 = require('gulp-livereload'),
		svgstore 			 = require('gulp-svgstore'),
		inject 				 = require('gulp-inject'),
    svgmin 				 = require('gulp-svgmin');

sass.compiler = require('node-sass');

gulp.task('js', function() {
	return gulp.src('app/js/*.js')
	.pipe(concat('common.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('public/js'))
	.pipe(livereload())
});

gulp.task('vendorjs', function() {
	return gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/bootstrap/dist/js/bootstrap.min.js',
		'node_modules/jquery-modal/jquery.modal.min.js',
		'node_modules/owl.carousel2/dist/owl.carousel.min.js'
		])
	.pipe(concat('vendor.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('public/js'))
	.pipe(livereload())
});

gulp.task('scss', function() {
	return gulp.src('app/scss/**/*.scss')
	.pipe(sassGlob())
	.pipe(sass().on('error', sass.logError))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS())
	.pipe(gulp.dest('public/css'))
	.pipe(livereload())
});

// gulp.task('svg', function() {
// 	return gulp.src('app/img/svg/*.svg')
// 	.pipe(gulp.dest('public/img/svg'))
// });


gulp.task('svgstore', function () {
		var svgs = gulp.src('app/img/svgSprite/*.svg')
    .pipe(svgmin({
			plugins: [{
				cleanupIDs: {
						minify: true
				}
			},
			{
				removeUselessStrokeAndFill: {
					stroke: true,
					fill: true,
					removeNone: true,
					hasStyleOrScript: true,
				}
			}]
		}))
    .pipe(svgstore({ inlineSvg: true }))
    .pipe(gulp.dest('public/img/svg'));

		function fileContents (filePath, file) {
        return file.contents.toString();
    }

		return gulp.src('app/index.html')
        	.pipe(inject(svgs, { transform: fileContents }))
        	.pipe(gulp.dest('public'));
});

gulp.task('fonts', function() {
	return gulp.src('app/fonts/**/*.**')
	.pipe(gulp.dest('public/fonts'))
});


gulp.task('moveImg', function() {
	return gulp.src('app/img/*.**')
	.pipe(gulp.dest('public/img'))
})

gulp.task('html', function() {
	return gulp.src(['app/*.html', 'app/*.php'])
	.pipe(gulp.dest('public'))
	.pipe(livereload())
});

gulp.task('watch', ['scss', 'js', 'svgstore'], function() {
	livereload.listen()
	gulp.watch('app/scss/**/*.scss', ['scss']);
	gulp.watch('app/js/*.js', ['js']);
	gulp.watch('app/*.html', ['html']);
	gulp.watch('app/img/svg/*.svg', ['svgstore']);
	gulp.watch('app/img/*.**', ['moveImg']);
});

gulp.task('default', ['html', 'scss', 'vendorjs', 'js', 'svgstore', 'moveImg', 'fonts']);

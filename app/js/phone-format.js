
;
(function ($) {
    $.fn.ruPhoneFormat = function (options) {
        var params = $.extend({
            format: '+7 (xxx) - xxx - xx - xx',
            international: false,

        }, options);

        $(this).on('keypress', function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
            var curchr = this.value.length;
            var curval = $(this).val();
            if (curchr == 0 && e.which != 8 && e.which != 0) {
              $(this).val('+7(' + curval)
            } else if (curchr == 6 && e.which != 8 && e.which != 0) {
                $(this).val(curval + ')');
            } else if (curchr == 10 && e.which != 8 && e.which != 0) {
                $(this).val(curval + "-");
            } else if (curchr == 13 && e.which != 8 && e.which != 0) {
                $(this).val(curval + "-");
            }
            $(this).attr('maxlength', '16');
        });
        $(this).bind('paste', function (e) {
            e.preventDefault();
            var inputValue = e.originalEvent.clipboardData.getData('Text');
            if (!$.isNumeric(inputValue)) {
                return false;
            } else {
                inputValue = String(inputValue.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3"));
                $(this).val(inputValue);
                $(this).val('');
                inputValue = inputValue.substring(0, 14);
                $(this).val(inputValue);
            }
        });
    }
}(jQuery));

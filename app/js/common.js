$(function() {

  $('.review__more').click(function(){
    $(this).prev(".review__body").addClass("open");
    $(this).addClass("invisible");
  });

  $(".input[type='phone']").ruPhoneFormat();

  $("#carouselReviews").owlCarousel({
    autoplayHoverPause: true,
    autoplay: true,
    nav: true,
    loop: true,
    margin:20,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
    },
  });

  $(".ajax-form").submit(function(){
    // собираем данные с формы
    var str = $(this).serialize();
    var form = $(this);
    // отправляем данные
    $.ajax({
      url: "/form.php", // куда отправляем
      type: "post", // метод передачи
      data: str,
      success: function(msg) {
        if(msg == 'OK') {
          form.html('<div class="ok-message">Вашa заявка была отправлена</div>');
        }
        else {
          form.find(".error").html(msg);
        }
      }
    });
    return false;
  });

  $('.tarif-btn').click(function(event) {
  event.preventDefault();
  this.blur(); // Manually remove focus from clicked link.
  var tarif = $(this).attr("data");
  $('#tarif-modal').find(".tarif-card__title span").text(tarif);
  $('#tarif-modal').find("input[name='tarif']").attr("value", tarif);
  $('#tarif-modal').modal();
});

});
